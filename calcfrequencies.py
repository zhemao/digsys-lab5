from __future__ import division
import csv
import sys

BASE_FREQ = 1E6

if __name__ == '__main__':
    rdr = csv.reader(sys.stdin, delimiter=' ')
    print "note\tdiv\tfreq"
    for hexnum, note in rdr:
        div = int(hexnum[2:6], 16)
        freq = 0.5 * BASE_FREQ / div
        output = "%s\t0x%04X\t%0.3f" % (note, div, freq)
        print output
