import numpy as np
import matplotlib.pyplot as plt

IN_PERIOD = 20
DIV = 50
OUT_PERIOD = DIV * IN_PERIOD * 2

def clock_out(x):
    val = x % (OUT_PERIOD)
    return 5 if val < DIV * IN_PERIOD else 0

def one_shot(x):
    val = x % (OUT_PERIOD)
    return 5 if val < IN_PERIOD else 0


if __name__ == '__main__':
    t = np.array(range(0, OUT_PERIOD * 3, IN_PERIOD/4))
    CLK = [clock_out(x) for x in t]
    ONES = [one_shot(x) for x in t]

    plt.subplot(211)
    plt.plot(t, CLK)
    plt.ylim(0, 6)
    plt.xlabel('Time (ns)')
    plt.ylabel('Voltage (V)')
    plt.subplot(212)
    plt.plot(t, ONES)
    plt.ylim(0, 6)
    plt.xlabel('Time (ns)')
    plt.ylabel('Voltage (V)')
    plt.show()
