from __future__ import division
import matplotlib.pyplot as plt
import numpy as np

BASE_FREQ = 10**6

if __name__ == '__main__':
    div = np.array(range(1, 2**16))
    freq = 0.5 * BASE_FREQ / div
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(div, freq)
    ax.set_yscale('log')
    ax.set_xscale('log')
    plt.show()
