Note Encoding

Each switch corresponds to a certain note. When the switch is at '1', the note
is being played. The switches correspond to notes in the following way.

	7 -> C
	6 -> D
	5 -> E
	4 -> F
	3 -> G
	2 -> A
	1 -> B

Switch 0 is left unused. Only one of the note switches can high at a given 
time.

Clock Divider

Count is incremented by 1 each positive edge of the input CLK if EN = 1 and 
RST = 0.

Once count reaches DIV, count resets to 1 and CLK_OUT is toggled. 
ONE_SHOT is set to 1 for one cycle of the input clock after each positive edge 
of CLK_OUT. 

For a 50 MHz input clock and DIV = 50, this would mean CLK_OUT would be a 
square wave with a frequency of 500 KHz and a duty cycle of 0.5, while 
ONE_SHOT would be a square wave with a frequency of 500 KHz and a duty cycle
of 0.01

Note Decoder

FREQ = 0.5 * 1 MHz / DIV

The following table (note-encodings.csv) shows the notes, the given DIV values,
and the calculated frequencies. As you can see, these frequencies correspond
quite closely to the ideal frequencies.

Seven Segment Display

The seven segment display module gets passed in a five-bit binary number
from the top-level piano module. The lower four bits represent the note,
and the most significant bit represents the octave. The module then decodes
this to figure out which digits to output on the seven segment display.
It generates a 9-bit number, 4 bits for each of two seven segment displays
and 1 bit for whether or not a point is to be displayed. The point
is only displayed for the black notes of the piano. The first seven segment
display shows the note (A-G), and the second shows the octave (3 or 4). 
For the notes A-F, it uses to binary number corresponding to that
hexadecimal digit. It represents the note G using the digit 6. 
The decoder is multiplexed, so that only one of the four Seven-Segment displays
is lit up on each cycle. Another process takes the bits of the 9-bit number
corresponding to that display and uses it to produce the outputs for the
display.

Simulation

The inputs pb_in and switch_in are displaying the correct values. Of note
is the section where seg_out goes to 00010001 and then 00001101.
There outputs correspond to values A and 3, without the point, on the
seven segment displays. This is the correct output, since the note being
played at that point is indeed an A3. The seg_out is not set to the indicative
values when A#3 is being played because the multiplexer is on the unused 
digits during that time period. 
